FROM ruby:2.3
MAINTAINER kfrz.code@gmail.com

# Install Dependencies
RUN apt-get update

RUN apt-get install -y build-essential

# for js runtime
RUN apt-get install -y nodejs

# for postgres
RUN apt-get install -y libpq-dev

# for nokogiri
RUN apt-get install -y libxml2-dev libxslt1-dev

# for capybara-webkit
RUN apt-get install -y libqt4-webkit libqt4-dev xvfb

RUN mkdir -p /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install --jobs 20 --retry 5

COPY . ./

CMD bundle exec rails s -b 0.0.0.0