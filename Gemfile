source 'https://rubygems.org'

# Base
gem 'rails', '~> 5.0.0'
gem 'puma', '~> 3.4.0'
gem 'rack-canonical-host'
gem 'rack-cors'

# DB
gem 'pg', '~> 0.18.4'
gem 'redis', '~> 3.0'

# Infrastructure
gem 'bcrypt', '~> 3.1.7'
# gem 'cancancan', '~> 1.10'
# gem 'devise'
gem 'jbuilder', '~> 2.5'
gem 'recipient_interceptor'
gem 'resque'
gem 'carrierwave'

# Monitoring
gem 'honeybadger' # exception notification
gem 'skylight' # monitoring performance
gem 'lograge' # goodness with logs

# Assets
# gem 'autoprefixer-rails'
# gem 'bourbon'
# gem 'execjs', '~> 2.7'
# gem 'jquery-rails'
# gem 'neat'
# gem 'normalize-rails', '~> 3.0'
# gem 'sass-rails'
# gem 'simple_form'
# gem 'title'
# gem 'uglifier'

group :development do
  gem 'better_errors'
  gem 'listen', '~> 3.0.5'
  gem 'guard', '2.13.0'
  gem 'guard-rspec', '~> 4.7.2'
  gem 'refills'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console'
end

group :development, :test do
  gem 'awesome_print'
# gem 'benchmark-bigo'
  gem 'bullet' # help kill n+1
  gem 'bundler-audit', '>= 0.5', require: false
  gem 'dotenv-rails'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'rspec'
  gem 'rspec-rails'
end

group :development, :staging do
  gem 'rack-mini-profiler', require: false
end

group :test do
  gem 'capybara-webkit'
  gem 'codeclimate-test-reporter', require: false
  gem 'database_cleaner'
# gem 'formulaic'
  gem 'shoulda-matchers'
  gem 'timecop'
  gem 'webmock'
end

group :staging, :production do
  gem 'rack-timeout'
  gem 'rails_stdout_logging'
  gem 'rails_12factor'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
