# DISINTEREST  

* Ruby 2.3.0  + Rails 5.0.0

You'll need docker to get this running, and docker-compose.

On Debian Linux it's as easy as installing docker and cloning the repo.

You'll need to copy `disinterest.env.example` to `.disinterest.env` as well for proper container link.

```
> docker-compose build && docker-compose up...
```

Then to execute commands to the rails server, you can use Docker's exec command as such:

```
> docker-compose exec web rake db:setup
> docker-compose exec web bundle exec rspec features/
```


Happy boarding!

